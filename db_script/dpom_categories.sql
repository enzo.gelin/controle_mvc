-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : databaseCMS:3306
-- Généré le :  ven. 15 mai 2020 à 13:42
-- Version du serveur :  5.7.28
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mvc-pa`
--

-- --------------------------------------------------------

--
-- Structure de la table `dpom_categories`
--

CREATE TABLE `dpom_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT 'Page',
  `content` text,
  `order_page` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `dpom_categories`
--

INSERT INTO `dpom_categories` (`id`, `name`, `content`, `order_page`) VALUES
(0, 'NULL', 'NULL', 0),
(1, 'Francais', NULL, 1),
(2, 'Maths', NULL, 2),
(3, 'Anglais', NULL, 3),
(4, 'Histoire', NULL, 4),
(5, 'Page', NULL, 5),
(7, 'Page', NULL, 7),
(8, 'Page', NULL, 8),
(9, 'Page', NULL, 9),
(10, 'Page', NULL, 10),
(11, 'Page', NULL, 11),
(12, 'Page', NULL, 13),
(14, 'Page', NULL, 14),
(15, 'Page', NULL, 15),
(16, 'Page', NULL, 16),
(17, 'Page', NULL, 17),
(18, 'Page', NULL, 18),
(19, 'Page', NULL, 19),
(20, 'dddd', NULL, 20),
(21, 'Page', NULL, 21);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dpom_categories`
--
ALTER TABLE `dpom_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_page` (`order_page`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

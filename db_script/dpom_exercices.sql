-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : databaseCMS:3306
-- Généré le :  ven. 15 mai 2020 à 13:42
-- Version du serveur :  5.7.28
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mvc-pa`
--

-- --------------------------------------------------------

--
-- Structure de la table `dpom_exercices`
--

CREATE TABLE `dpom_exercices` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `content` varchar(255) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `dpom_exercices`
--

INSERT INTO `dpom_exercices` (`id`, `title`, `content`, `id_categorie`, `date`) VALUES
(9, 'Histoire_1', '<p>Exercices histoires</p>', 4, '2020-05-14 19:27:12');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dpom_exercices`
--
ALTER TABLE `dpom_exercices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categorie` (`id_categorie`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `dpom_exercices`
--
ALTER TABLE `dpom_exercices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `dpom_exercices`
--
ALTER TABLE `dpom_exercices`
  ADD CONSTRAINT `dpom_exercices_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `dpom_categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

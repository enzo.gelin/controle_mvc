<?php 
namespace App\Managers;

use App\Core\Connection\PDOConnection;
use App\Core\Manager;
use App\Core\QueryBuilder;
use App\Models\Post;

class PostManager extends Manager {

    public function __construct()
    {
        parent::__construct(Post::class, 'posts');
    }

    // Fonction retourne les posts d'un utilisateur
    public function getUserPost(int $id)
    {
        return (new QueryBuilder())
            ->select('p.*,u.*')
            ->from('dpom_posts','p')
            ->join('dpom_users', 'u')
            ->where('p.author = :iduser')
            ->setParameter('iduser', $id)
            ->getQuery()
            ->getArrayResult(Post::class)
            ;
    }
}
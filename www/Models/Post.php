<?php

namespace App\Models;

use App\Core\Helper;
use App\Core\Model;

class Post extends Model
{
    protected $id;
    protected $title;
    protected $author;

    public function setId($id): self
    {
        $this->id=$id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setTitle($title)
    {
        $this->title=$title;
    }
    // setAuthor() prend en paramètre un objet User | Author = l’utilisateur associé à l’ID de la table utilisateur 


    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function initRelation(): array {
        return [
            'author' => User::class
        ];
    }

 
}













<?php
namespace App\Core;

use App\Core\Connection\BDDInterface;
use App\Core\Connection\PDOConnection;
use App\Core\Connection\ResultInterface;

class QueryBuilder
{
    protected $connection;
    protected $query;
    protected $parameters;
    protected $alias;

    // Database connection
    public function __construct(BDDInterface $connection = NULL)
    {
        $this->connection = new PDOConnection();
    }
    // Fonction SELECT
    public function select(string ...$value)
    {
        $this->select = $value;
        return $this;
    }
    // Fonction FROM
    public function from(string $table, string $alias = null): self
    {
        if ($alias) {
            $this->from[$alias] = $table;
        } else {
            $this->from[] = $table;
        }
        return $this;
    }
    // Fonction WHERE
    public function where(string $conditions): QueryBuilder
    {
        $this->where[] = $conditions;
        return $this;
    }
    // Fonction SET PARAMETER
    public function setParameter(string $key, string $value): QueryBuilder
    {
        $this->parameter[$key] = $value;
        return $this;
    }
    // Fonction JOIN
    public function join(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        // Si un alias est déclaré on le set pour le join
        if ($aliasTarget) {
            $this->join[$aliasTarget] = $table;
        } else {
            $this->join[] = $table;
        }
        return $this; 
    }
    // Fonction LEFT JOIN
    public function leftJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        // Si un alias est déclaré on le set pour le join
        if ($aliasTarget) {
            $this->leftjoin[$aliasTarget] = $table;
        } else {
            $this->leftjoin[] = $table;
        }
        return $this;  
    }
    // Fonction RIGHT JOIN
    public function rightJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        // Si un alias est déclaré on le set pour le join
        if ($aliasTarget) {
            $this->rightjoin[$aliasTarget] = $table;
        } else {
            $this->rightjoin[] = $table;
        }
        return $this; 
    }
    // Fonction ADD TO QUERY
    public function addToQuery(string $query): QueryBuilder
    {
        return $this;  
    }
    // Fonction GET QUERY
    public function getQuery(): ResultInterface
    {
        error_log(print_r($this,true));
        $request_sql = $this::__toString();
        error_log('requete SQL => ' . $request_sql);
        $queryPrepared = $this->pdo->prepare($request_sql);
        $queryPrepared->execute();

        return $queryPrepared;
        
    }
    // Fonction toString : permet de convertir FROM en chaine de caracteres
    public function __toString()
    {
        $parts = ['SELECT'];
        if ($this->select) {
            $parts[] = join(', ', $this->select);
        } else {
            $parts[] = '*';
        }
        $parts[] = 'FROM';
        $parts[] = $this->buildFrom();
        $parts[] = 'JOIN';
        $parts[] = $this->buildJoin();
        if ($this->where) {
            $parts[] = 'WHERE';
            $parts[] = '(' . join(') AND (', $this->where) . ')';
        }
        return join(' ', $parts);
    }
    // Fonction BuildForm : fabrique avec ces petites mains de maître le FROM
    private function buildFrom(): string
    {
        $from = [];
        foreach ($this->from as $key => $value) {
            if (is_string($key)) {
                $from[] = $value.' AS '. $key;
            } else {
                $from[] = $value;
            }
        }
        return join(', ', $from);
    }
    // Fonction BuildJoin : fabrique avec ces petites mains de maître le FROM
    private function buildJoin(): string
    {
        $join = [];
        foreach ($this->join as $key => $value) {
            if (is_string($key)) {
                $join[] = $value.' AS '. $key;
            } else {
                $join[] = $value;
            }
        }
        return join(', ', $join);
    }

}